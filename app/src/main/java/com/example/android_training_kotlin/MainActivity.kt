package com.example.android_training_kotlin

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import rx.Observer
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private var subscription: Subscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val service: GetStarredRepos? = getRetrofitInstance()?.create(
            GetStarredRepos::class.java
        )

        subscription = service!!.getStarredRepos()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<List<GithubRepoModel?>?> {
                override fun onCompleted() {
                    Log.d("@@TAG", "In onCompleted()")
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    Log.d("@@TAG", "In onError()")
                }

                override fun onNext(gitHubRepos: List<GithubRepoModel?>?) {
                    Log.d("@@TAG", "In onNext()")

                    val recyclerView = findViewById<RecyclerView>(R.id.repoList)
                    val adapter = GithubRepoAdapter(gitHubRepos)
                    val layoutManager: RecyclerView.LayoutManager =
                        LinearLayoutManager(this@MainActivity)
                    recyclerView.layoutManager = layoutManager
                    recyclerView.adapter = adapter
                }
            })
    }
}
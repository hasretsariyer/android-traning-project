package com.example.android_training_kotlin

import retrofit2.http.GET
import rx.Observable

interface GetStarredRepos {
    @GET("starred")
    fun getStarredRepos(): Observable<List<GithubRepoModel?>?>?
}
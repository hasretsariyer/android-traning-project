package com.example.android_training_kotlin

data class GithubRepoModel(
    val name: String,
    val description: String,
    val language: String,
    val stargazers_count: Int
)